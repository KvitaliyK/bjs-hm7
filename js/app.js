// 1. Як можна створити рядок у JavaScript?
// За допогою двойних лапок, одинарних лапок, зворотніх лапок, за допомогою конструкції String

// 2. Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?
// Одинарні та подвійні лапки мають однакові властивості, а в зворотніх дозволяється вбудовувати змінні

// 3. Як перевірити, чи два рядки рівні між собою?
// За допомогою оператора рівності(===)

// 4. Що повертає Date.now()?
// Повертає кількість мілісекунд, що пройшли з 1 січня 1970 року 00:00:00 UTC до моменту виклику методу.

// 5. Чим відрізняється Date.now() від new Date()?
// Date.now() повертає кількість мілісекунд, що пройшли з 1 січня 1970 року 00:00:00 UTC до моменту виклику методу.
// Date() повертає поточний час.


// Практичні завдання
// 1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, 
// якщо рядок є паліндромом (читається однаково зліва направо і справа наліво), або false в іншому випадку.
function isPalindrome(str) {
    return str.split('').reverse().join('') === str;
}
console.log(isPalindrome('34543'));
console.log(isPalindrome('34567'));


// 2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок, який потрібно перевірити,
// максимальну довжину і повертає true, якщо рядок менше або дорівнює вказаній довжині, і false,
// якщо рядок довший. Ця функція стане в нагоді для валідації форми. Приклади використання функції:
// Рядок коротше 20 символів
// funcName('checked string', 20); // true
// Довжина рядка дорівнює 18 символів
// funcName('checked string', 10); // false

function funcName(str, count) {
    if (str.length > count) {
        return false;
    }
    return true
}

console.log(funcName('checked string', 20));
console.log(funcName('checked string', 10));

// 3. Створіть функцію, яка визначає скільки повних років користувачу. Отримайте дату народження користувача через prompt.
// Функція повина повертати значення повних років на дату виклику функцію.
function calculateAge() {
    const birthDateInput = prompt("Введіть вашу дату народження у форматі YYYY-MM-DD:");
    const birthDate = new Date(birthDateInput);
    const currentDate = new Date();

    const years = currentDate.getFullYear() - birthDate.getFullYear();
    const birthMonth = birthDate.getMonth();
    const currentMonth = currentDate.getMonth();

    if (birthMonth > currentMonth || (birthMonth === currentMonth && birthDate.getDate() > currentDate.getDate())) {
        return years - 1;
    }
    return years;
}

console.log("Повні роки користувача:", calculateAge());
